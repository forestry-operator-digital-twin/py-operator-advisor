# Operator Advisor Python Interface

A python interface to transform raw data from [Operator twin](https://gitlab.com/forestry-operator-digital-twin/simulator) simulation to an observation using [Operator Advisor](https://gitlab.com/forestry-operator-digital-twin/operator-advisor).