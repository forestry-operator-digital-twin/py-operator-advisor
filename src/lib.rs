use operator_advisor::{observation, InternalData, Metadata};
use pyo3::prelude::*;
use pyo3::PyObjectProtocol;
use serde::{Deserialize, Serialize};
use serde_json;

#[pyfunction]
/// convert the raw data into a discrete observation
pub fn convert_to_observation(data_string: String) -> PyResult<RespObservation> {
    match convert_instant_data_to_observation(data_string) {
        Ok(r) => Ok(r),
        Err(e) => Err(PyErr::new::<pyo3::exceptions::PyTypeError, _>(e)),
    }
}

pub fn convert_instant_data_to_observation(data_string: String) -> Result<RespObservation, String> {
    let data = InstantData::new(&data_string)?;
    let obs = observation::new_observation(&OBSERVATION_TYPE, &data.data);
    let tree_observation =
        if let observation::TreeObservation::NumberOfTreesClose(t) = obs.tree_observation() {
            t
        } else {
            return Err(String::from("not able to convert to discrete observation"));
        };
    Ok(RespObservation {
        tree_observation: tree_observation,
        excavator_speed_observation: obs.excavator_speed_observation(),
        excavator_angular_speed_observation: obs.excavator_angular_speed_observation(),
        stick_extension_observation: obs.stick_extension_observation(),
        stick_angle_observation: obs.stick_angle_observation(),
        finger_state_observation: obs.finger_state_observation(),
    })
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct InstantData {
    pub data: [InternalData; 2],
    pub metadata: Metadata,
}

impl InstantData {
    pub fn new(json_string: &String) -> Result<Self, String> {
        let json: Result<Self, serde_json::error::Error> =
            serde_json::from_str(json_string.as_str());
        match json {
            Ok(j) => Ok(j),
            Err(e) => Err(format!("{:?}", e)),
        }
    }
}

#[pyclass]
#[derive(Debug)]
pub struct RespObservation {
    #[pyo3(get)]
    pub tree_observation: String,
    #[pyo3(get)]
    pub excavator_speed_observation: String,
    #[pyo3(get)]
    pub excavator_angular_speed_observation: String,
    #[pyo3(get)]
    pub stick_extension_observation: String,
    #[pyo3(get)]
    pub stick_angle_observation: String,
    #[pyo3(get)]
    pub finger_state_observation: String,
}

#[pyproto]
impl PyObjectProtocol for RespObservation {
    fn __str__(&self) -> PyResult<&'static str> {
        Ok("RespObservation")
    }

    fn __repr__<'a>(&'a self) -> PyResult<String> {
        Ok(format!("{:?}", self))
    }
}

#[pymodule]
fn py_operator_advisor(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(convert_to_observation, m)?)?;
    m.add_class::<RespObservation>()?;
    Ok(())
}
static OBSERVATION_TYPE: observation::ObservationType =
    observation::ObservationType::DiscreteObservation;

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::io::prelude::*;
    use std::path::PathBuf;

    #[test]
    fn test_new_data() -> Result<(), String> {
        let json_string = get_string_from_json_file(PathBuf::from("src/test_data_step.json"))?;
        let data = InstantData::new(&json_string);
        data.expect("should be valid");
        Ok(())
    }
    #[test]
    fn test_with_string() {
        let json_string = r#"{
            "data": [
                {
                    "step": 0,
                    "tree_1": [
                        14.648051177527975,
                        0.0,
                        0.0
                    ],
                    "tree_2": [
                        5.9506321601061245,
                        17.55998261082101,
                        -2.6649393108969222e-15
                    ],
                    "tree_3": [
                        6.1239772240352295,
                        3.2709937537802034,
                        2.875986786226122e-15
                    ],
                    "tree_4": [
                        10.12397722403523,
                        8.354226085860319,
                        2.0784069763066506e-15
                    ],
                    "tree_5": [
                        7.7299572819785,
                        -6.337101230230758,
                        7.703082567114347e-15
                    ],
                    "tree_6": [
                        13.465262583874676,
                        -7.705233840324809,
                        8.226881535813113e-15
                    ],
                    "tree_7": [
                        14.071684881276383,
                        -16.99285350718128,
                        1.1782710530219623e-14
                    ],
                    "tree_8": [
                        6.804240545097969,
                        -21.9477398110094,
                        7.605294667123796e-15
                    ],
                    "tree_9": [
                        1.937219118293342,
                        11.02492378052426,
                        -2.4737817618847988e-15
                    ],
                    "tree_10": [
                        5.570553592771489,
                        -13.373359011629361,
                        6.867268418675934e-15
                    ],
                    "position_excavator": [
                        -3.4806913712401646,
                        9.589084048064467e-06,
                        -0.0027241749986571806
                    ],
                    "position_stick": [
                        1.88518003909897,
                        5.398527912385976e-05,
                        5.427614556892415
                    ],
                    "position_boom": [
                        -3.400494843867893,
                        6.297941312457395e-06,
                        1.7059488466287593
                    ],
                    "position_hand": [
                        1.519081285046342,
                        -0.017662165083496717,
                        1.9517369773919735
                    ],
                    "position_finger_1": [
                        1.9197433569241165,
                        -0.27750003140245083,
                        1.9538918734526216
                    ],
                    "position_finger_2": [
                        1.9752914579190173,
                        0.221998330255861,
                        1.9460877580250135
                    ],
                    "speed_excavator": [
                        0.00018556239190371774,
                        0.000570510182103724,
                        -0.16344531321705666
                    ],
                    "angular_speed_chassis": -0.0005358784087114204,
                    "angular_pos_chassis": -8.93130681198883e-06
                },
                {
                    "step": 1,
                    "tree_1": [
                        14.648051177527975,
                        0.0,
                        0.0
                    ],
                    "tree_2": [
                        5.9506321601061245,
                        17.55998261082101,
                        -2.6649393108969222e-15
                    ],
                    "tree_3": [
                        6.1239772240352295,
                        3.2709937537802034,
                        2.875986786226122e-15
                    ],
                    "tree_4": [
                        10.12397722403523,
                        8.354226085860319,
                        2.0784069763066506e-15
                    ],
                    "tree_5": [
                        7.7299572819785,
                        -6.337101230230758,
                        7.703082567114347e-15
                    ],
                    "tree_6": [
                        13.465262583874676,
                        -7.705233840324809,
                        8.226881535813113e-15
                    ],
                    "tree_7": [
                        14.071684881276383,
                        -16.99285350718128,
                        1.1782710530219623e-14
                    ],
                    "tree_8": [
                        6.804240545097969,
                        -21.9477398110094,
                        7.605294667123796e-15
                    ],
                    "tree_9": [
                        1.937219118293342,
                        11.02492378052426,
                        -2.4737817618847988e-15
                    ],
                    "tree_10": [
                        5.570553592771489,
                        -13.373359011629361,
                        6.867268418675934e-15
                    ],
                    "position_excavator": [
                        -3.4806782238295093,
                        1.4562903752502604e-05,
                        -0.01633795976733142
                    ],
                    "position_stick": [
                        1.88516415050287,
                        8.52428695101732e-05,
                        5.414033577523684
                    ],
                    "position_boom": [
                        -3.4004878476980798,
                        1.0980229869561597e-05,
                        1.6923353526232279
                    ],
                    "position_hand": [
                        1.5287440682193807,
                        -0.030783426735686334,
                        1.9275937773072724
                    ],
                    "position_finger_1": [
                        1.9287102629999688,
                        -0.2839278063028375,
                        1.9264424093279973
                    ],
                    "position_finger_2": [
                        1.9691880906948913,
                        0.21835258979505534,
                        1.92088212643396
                    ],
                    "speed_excavator": [
                        0.0003971444254500873,
                        6.918212104840916e-05,
                        -0.4900778423523836
                    ],
                    "angular_speed_chassis": 0.0002441462077118389,
                    "angular_pos_chassis": -5.717511157731268e-06
                }
            ],
            "metadata": {
                "time_interval": 1,
                "unit_of_time": "s",
                "unit_of_distance": "m",
                "unit_of_angle": "rad",
                "starting_time_of_recording_unix": 1634840410591.388
            }
        }"#;
        let data = InstantData::new(&String::from(json_string));
        data.expect("should be valid");
    }
    #[test]
    fn test_with_function() {
        let json_string = r#"{
            "data": [
                {
                    "step": 0,
                    "tree_1": [
                        14.648051177527975,
                        0.0,
                        0.0
                    ],
                    "tree_2": [
                        5.9506321601061245,
                        17.55998261082101,
                        -2.6649393108969222e-15
                    ],
                    "tree_3": [
                        6.1239772240352295,
                        3.2709937537802034,
                        2.875986786226122e-15
                    ],
                    "tree_4": [
                        10.12397722403523,
                        8.354226085860319,
                        2.0784069763066506e-15
                    ],
                    "tree_5": [
                        7.7299572819785,
                        -6.337101230230758,
                        7.703082567114347e-15
                    ],
                    "tree_6": [
                        13.465262583874676,
                        -7.705233840324809,
                        8.226881535813113e-15
                    ],
                    "tree_7": [
                        14.071684881276383,
                        -16.99285350718128,
                        1.1782710530219623e-14
                    ],
                    "tree_8": [
                        6.804240545097969,
                        -21.9477398110094,
                        7.605294667123796e-15
                    ],
                    "tree_9": [
                        1.937219118293342,
                        11.02492378052426,
                        -2.4737817618847988e-15
                    ],
                    "tree_10": [
                        5.570553592771489,
                        -13.373359011629361,
                        6.867268418675934e-15
                    ],
                    "position_excavator": [
                        -3.4806913712401646,
                        9.589084048064467e-06,
                        -0.0027241749986571806
                    ],
                    "position_stick": [
                        1.88518003909897,
                        5.398527912385976e-05,
                        5.427614556892415
                    ],
                    "position_boom": [
                        -3.400494843867893,
                        6.297941312457395e-06,
                        1.7059488466287593
                    ],
                    "position_hand": [
                        1.519081285046342,
                        -0.017662165083496717,
                        1.9517369773919735
                    ],
                    "position_finger_1": [
                        1.9197433569241165,
                        -0.27750003140245083,
                        1.9538918734526216
                    ],
                    "position_finger_2": [
                        1.9752914579190173,
                        0.221998330255861,
                        1.9460877580250135
                    ],
                    "speed_excavator": [
                        0.00018556239190371774,
                        0.000570510182103724,
                        -0.16344531321705666
                    ],
                    "angular_speed_chassis": -0.0005358784087114204,
                    "angular_pos_chassis": -8.93130681198883e-06
                },
                {
                    "step": 1,
                    "tree_1": [
                        14.648051177527975,
                        0.0,
                        0.0
                    ],
                    "tree_2": [
                        5.9506321601061245,
                        17.55998261082101,
                        -2.6649393108969222e-15
                    ],
                    "tree_3": [
                        6.1239772240352295,
                        3.2709937537802034,
                        2.875986786226122e-15
                    ],
                    "tree_4": [
                        10.12397722403523,
                        8.354226085860319,
                        2.0784069763066506e-15
                    ],
                    "tree_5": [
                        7.7299572819785,
                        -6.337101230230758,
                        7.703082567114347e-15
                    ],
                    "tree_6": [
                        13.465262583874676,
                        -7.705233840324809,
                        8.226881535813113e-15
                    ],
                    "tree_7": [
                        14.071684881276383,
                        -16.99285350718128,
                        1.1782710530219623e-14
                    ],
                    "tree_8": [
                        6.804240545097969,
                        -21.9477398110094,
                        7.605294667123796e-15
                    ],
                    "tree_9": [
                        1.937219118293342,
                        11.02492378052426,
                        -2.4737817618847988e-15
                    ],
                    "tree_10": [
                        5.570553592771489,
                        -13.373359011629361,
                        6.867268418675934e-15
                    ],
                    "position_excavator": [
                        -3.4806782238295093,
                        1.4562903752502604e-05,
                        -0.01633795976733142
                    ],
                    "position_stick": [
                        1.88516415050287,
                        8.52428695101732e-05,
                        5.414033577523684
                    ],
                    "position_boom": [
                        -3.4004878476980798,
                        1.0980229869561597e-05,
                        1.6923353526232279
                    ],
                    "position_hand": [
                        1.5287440682193807,
                        -0.030783426735686334,
                        1.9275937773072724
                    ],
                    "position_finger_1": [
                        1.9287102629999688,
                        -0.2839278063028375,
                        1.9264424093279973
                    ],
                    "position_finger_2": [
                        1.9691880906948913,
                        0.21835258979505534,
                        1.92088212643396
                    ],
                    "speed_excavator": [
                        0.0003971444254500873,
                        6.918212104840916e-05,
                        -0.4900778423523836
                    ],
                    "angular_speed_chassis": 0.0002441462077118389,
                    "angular_pos_chassis": -5.717511157731268e-06
                }
            ],
            "metadata": {
                "time_interval": 1,
                "unit_of_time": "s",
                "unit_of_distance": "m",
                "unit_of_angle": "rad",
                "starting_time_of_recording_unix": 1634840410591.388
            }
        }"#;
        let data = convert_instant_data_to_observation(String::from(json_string));
        data.expect("should be valid");
    }
    fn get_string_from_json_file(path: PathBuf) -> Result<String, String> {
        match File::open(path) {
            Err(e) => Err(format!("{:}", e)),
            Ok(mut file) => {
                let mut json_string = String::new();
                if let Err(e) = file.read_to_string(&mut json_string) {
                    return Err(format!("{}", e));
                }
                Ok(json_string.clone())
            }
        }
    }
}
